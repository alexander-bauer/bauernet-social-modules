#######################
BauerNet Social Modules
#######################

This repository houses Terraform modules related to social and communications
applications.

.. toctree::
   :maxdepth: 1

   */README
   CHANGELOG
